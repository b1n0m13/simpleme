import ru.b1n0m.simpleme.Canvas.*;

import javax.microedition.lcdui.Graphics;
import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

public class SimpleDefenceEngine extends SimpleTouchProcessor implements IDrawable, ITask, ICycleChangesCatcher, IKeyProcessor {
  private MainView mainView;

  private byte menu;
  private byte mode;
  private static final byte MODE_MENU = 0;
  private static final byte MODE_GAME = 1;

  private final String startString;
  private final String exitString;

  private Tile[][] map;
  private int MAP_W;
  private int MAP_H;
  private int offsetX;
  private int offsetY;

  private int powerUp, powerDown, powerLeft, powerRight;
  private final int MAX_CURSOR_SPEED = 5;
  private Vector turrets;

  public SimpleDefenceEngine(MainView mainView) {
    this.mainView = mainView;
    startString = "START";
    exitString = "EXIT";
    mode = MODE_MENU;
    turrets = new Vector();
  }

  private static Tile[][] loadMap(int number) {
    byte[] mapBuffer;
    try {
      mapBuffer = getMapBuffer(number);
    } catch (IOException e) {
      return null;
    }

    int MAP_W = mapBuffer[0];
    int MAP_H = mapBuffer.length / MAP_W;
    Tile[][] map = new Tile[MAP_W][];
    for (int i = 0; i != MAP_W; ++i) {
      map[i] = new Tile[MAP_H];
      for (int j = 0; j != MAP_H; ++j)
        map[i][j] = new Tile((short) (i * Tile.SIZE), (short) (j * Tile.SIZE), mapDecode(mapBuffer[i + MAP_W * j + 1]));
    }
//    for (int i = 0; i != MAP_W; ++i) {
//      for (int j = 0; j != MAP_H; ++j)
//        map[i][j] = new Tile((short) (i * Tile.SIZE), (short) (j * Tile.SIZE), mapDecode(mapBuffer[i + MAP_W * j + 1]));
//    }
    return map;
  }

  private static byte mapDecode(byte code) {
    switch (code) {
      case 'S':
        return Tile.TYPE_SPACE;
      case 'B':
        return Tile.TYPE_BASE;
      case 'R':
        return Tile.TYPE_ROAD;
      case 'P':
        return Tile.TYPE_SPAWN;
      default:
        return Tile.TYPE_NONE;
    }
  }

  private static byte[] getMapBuffer(int number) throws IOException {
    byte[] buffer = new byte[1];
    InputStream is = buffer.getClass().getResourceAsStream(number + ".map");
    if (is == null)
      throw new IOException("cannot locate " + number + ".map");
    try {
      buffer = new byte[is.available()];
      if (is.read(buffer) != buffer.length)
        throw new IOException("unexpected eof " + number + ".map");
      return buffer;
    } finally {
      try {
        is.close();
      } catch (IOException ignored) {
      }
    }
  }

  public void showNotify() {

  }

  public void hideNotify() {

  }

  public void paint(Graphics graphics) {
    switch (mode) {
      case MODE_MENU: {
        graphics.setColor(0);
        graphics.fillRect(0, 0, mainView.DISPLAY_W, mainView.DISPLAY_H);
        graphics.setColor(menu == 0 ? 0x770000 : 0xFFFFFF);
        int fontHeight = graphics.getFont().getHeight();
        graphics.drawString(startString, (mainView.DISPLAY_W - graphics.getFont().stringWidth(startString)) / 2, mainView.DISPLAY_H / 2 - fontHeight - 3, 0);
        graphics.setColor(menu == 1 ? 0x770000 : 0xFFFFFF);
        graphics.drawString(exitString, (mainView.DISPLAY_W - graphics.getFont().stringWidth(exitString)) / 2, mainView.DISPLAY_H / 2 + 3, 0);
      }
      break;
      case MODE_GAME: {
        graphics.setColor(0);
        graphics.fillRect(0, 0, mainView.DISPLAY_W, mainView.DISPLAY_H);
        graphics.setColor(0xFFFFFF);
        graphics.drawRect(offsetX-1, offsetY-1, MAP_W*Tile.SIZE+1, MAP_H*Tile.SIZE+1);
        for (int i = 0; i != MAP_W; ++i) {
          for (int j = 0; j != MAP_H; ++j){
            map[i][j].paint(graphics, offsetX, offsetY);
          }
        }
        drawExit(graphics);
      }
      break;
    }
  }

  private void drawExit(Graphics graphics) {
    graphics.setColor(0xFFFFFF);
    paintCursor(graphics);
    graphics.drawString("Exit", mainView.DISPLAY_W - graphics.getFont().stringWidth("Exit") - 2, mainView.DISPLAY_H - graphics.getFont().getHeight() - 2, 0);
  }

  private void paintCursor(Graphics graphics) {
    int cx = mainView.DISPLAY_W/2;
    int cy = mainView.DISPLAY_H/2;
    graphics.drawLine(cx, cy,cx +10, cy+3);
    graphics.drawLine(cx, cy,cx +3, cy+10);
    graphics.drawLine(cx +10, cy+3,cx +3, cy+10);
  }

  public void update(int keyCode) {
    boolean upPressed = (keyCode & (KEY_CODE_UP | KEY_CODE_PAD_UP)) != 0;
    boolean downPressed = (keyCode & (KEY_CODE_DOWN | KEY_CODE_PAD_DOWN)) != 0;
    boolean rightPressed = (keyCode & (KEY_CODE_RIGHT | KEY_CODE_PAD_RIGHT)) != 0;
    boolean leftPressed = (keyCode & (KEY_CODE_LEFT | KEY_CODE_PAD_LEFT)) != 0;
    switch (mode) {
      case MODE_GAME: {
        powerUp = upPressed ? (powerUp == 0 ? 1 : Math.min(MAX_CURSOR_SPEED, powerUp + 1)) : Math.max(0, powerUp - 1);
        powerDown = downPressed ? (powerDown == 0 ? 1 : Math.min(MAX_CURSOR_SPEED, powerDown + 1)) : Math.max(0, powerDown - 1);
        powerLeft = leftPressed ? (powerLeft == 0 ? 1 : Math.min(MAX_CURSOR_SPEED, powerLeft + 1)) : Math.max(0, powerLeft - 1);
        powerRight = rightPressed ? (powerRight == 0 ? 1 : Math.min(MAX_CURSOR_SPEED, powerRight + 1)) : Math.max(0, powerRight - 1);

        offsetX += powerLeft - powerRight;
        offsetY += powerUp - powerDown;

        // process touch
        int[] pointerOffset = getPointerOffset();
        offsetX += pointerOffset[0];
        offsetY += pointerOffset[1];

        {
          int cursorX = scrToGameX(mainView.DISPLAY_W / 2);
          if (cursorX > MAP_W * Tile.SIZE) offsetX += cursorX - MAP_W * Tile.SIZE;
          else if (cursorX < 0) offsetX += cursorX;
        }
        {
          int cursorY = scrToGameY(mainView.DISPLAY_H / 2);
          if (cursorY > MAP_H * Tile.SIZE) offsetY += cursorY - MAP_H * Tile.SIZE;
          else if (cursorY < 0) offsetY += cursorY;
        }

        for (int i = 0, max = turrets.size(); i != max; ++i )
          ((Turret) turrets.elementAt(i)).update();
      }
      break;
      case MODE_MENU: {
        // process touch
        int[] pointerPress = getPointerPress();
        if (pointerPress != null) {
            // here we can calculate if menu items touched
        }
      }
      break;
    }
  }

  public int scrToGameX(int x){
    return x-offsetX;
  }

  public int scrToGameY(int y){
    return y-offsetY;
  }

  public int gameToScrX(int x){
    return x+offsetX;
  }

  public int gameToScrY(int y){
    return y+offsetY;
  }

  public Tile getTileAt(int x, int y) {
    try {
      return map[x / Tile.SIZE][y / Tile.SIZE];
    } catch (ArrayIndexOutOfBoundsException e) {
      return null;
    }
  }

  public boolean keyCodePressed(int keyCode) {
    switch (mode) {
      case MODE_MENU: {
        boolean upPressed = keyCode == KEY_CODE_UP || keyCode == KEY_CODE_PAD_UP;
        boolean downPressed = keyCode == KEY_CODE_DOWN || keyCode == KEY_CODE_PAD_DOWN;
        if (keyCode == KEY_CODE_FIRE) {
          switch (menu) {
            case 0:
              mode = MODE_GAME;
              map = loadMap(1);
              MAP_W = map.length;
              MAP_H = map[0].length;
              offsetX = -Tile.SIZE*MAP_W/2;
              offsetY = -Tile.SIZE*MAP_H/2;
              break;
            case 1:
              mainView.asyncStop(this);
              break;
          }
          return true;
        } else {
          if (upPressed ^ downPressed) {
            menu += downPressed ? 1 : -1;
            if (menu < 0)
              menu = 0;
            if (menu > 1)
              menu = 1;
            return true;
          }
        }
      }
      break;
      case MODE_GAME: {
        if (keyCode == KEY_CODE_SK_RIGHT) {
          mode = MODE_MENU;
          return true;
        }
        if (keyCode == KEY_CODE_FIRE) {
          Tile t =getTileAt(scrToGameX(mainView.DISPLAY_W/2), scrToGameY(mainView.DISPLAY_H / 2));
          if (t != null){
            switch (t.type) {
              case Tile.TYPE_SPACE:
                turrets.addElement(t.buildTurret(false));
                break;
            }
          }
          return true;
        }
        //other
      }
      break;
    }
    return false;
  }

  public void taskAdded(ITask task, boolean result) {

  }

  public void taskRemoved(ITask task, boolean result) {

  }

  public void stopped() {
    Midlet.midlet.destroyApp(true);
  }
}
