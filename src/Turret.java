import javax.microedition.lcdui.Graphics;

public class Turret {

  private int x;
  private int y;
  private short angle;
  private boolean isHeavy;
  private byte fireState, fireAnim;
  public byte turnSpeedLevel, turnSpeed;
  public byte fireSpeedLevel, fireSpeed;
  public byte damageLevel, damage;
  public byte radiusLevel, radius;
  public byte level;

  public static final byte[] turnSpeedTable = {3, 5, 8, 11, 14, 19};
  public static final byte[] turnSpeedHTable = {1, 2, 3, 5, 7, 10};

  public static final byte[] fireSpeedTable = {30, 28, 25, 20, 15, 10};
  public static final byte[] fireSpeedHTable = {100, 90, 80, 70, 60, 50};

  public static final byte[] dmgTable = {1, 2, 3, 4, 5, 6};
  public static final byte[] dmgHTable = {10, 15, 20, 30, 40, 50};

  public static final byte[] rTable = {15, 17, 20, 23, 27, 35};
  public static final byte[] rHTable = {25, 27, 30, 33, 37, 45};

  public Turret(int x, int y, boolean heavy) {
    this.x = x;
    this.y = y;
    isHeavy = heavy;
    level = 1;
    angle = 0;
    turnSpeedLevel = 0;
    fireSpeedLevel = 0;
    damageLevel = 0;
    radiusLevel = 0;
    fireState = 0;
    calcParams();
  }

  public void increaseLevel(int type) {
    switch (type) {
      case 0:
        level++;
        break;
      case 1:
        turnSpeedLevel++;
        break;
      case 2:
        fireSpeedLevel++;
        break;
      case 3:
        damageLevel++;
        break;
      case 4:
        radiusLevel++;
        break;
    }
    calcParams();
  }

  private void calcParams() {
    if (isHeavy) {
      turnSpeed = turnSpeedHTable[turnSpeedLevel];
      fireSpeed = (byte) (fireSpeedHTable[fireSpeedLevel] * (level == 0 ? 1 : 0.8));
      damage = (byte) (dmgHTable[damageLevel] * (level == 0 ? 1 : (level == 1 ? 1.2 : 1.5)));
      radius = rHTable[radiusLevel];
    } else {
      turnSpeed = turnSpeedTable[turnSpeedLevel];
      fireSpeed = (byte) (fireSpeedTable[fireSpeedLevel] * (level == 0 ? 1 : 0.7));
      damage = (byte) (dmgTable[damageLevel] + level);
      radius = rTable[radiusLevel];
    }
  }

  public void update() {
    if (traceTarget(0, 0)) {
      if (fireState == 0){
        fireAnim = 5;
        fireState += fireSpeed;
      }
    }
    if (fireAnim > 0)
      fireAnim--;
    if (fireState > 0)
      fireState--;
  }

  private boolean traceTarget(int ax, int ay) {
    short needAngle = dirTo(ax, ay);
    if (Math.min(Math.abs(angle - needAngle), Math.abs(angle+360-needAngle)) <= turnSpeed) {
      angle = needAngle;
      return true;
    }

    angle += (Math.abs(needAngle - angle) < 180 ? (angle < needAngle ? turnSpeed : -turnSpeed) : (angle < needAngle ? -turnSpeed : turnSpeed));
    if (angle >= 360)
      angle -= 360;
    if (angle < 0)
      angle += 360;
    return false;
  }

  private short dirTo(int ax, int ay) {
    int x1 = x + 12;
    int y1 = y + 12;
    byte quarter = (byte) (ax >= x1 ? (ay >= y1 ? 4 : 1) : (ay >= y1 ? 3 : 2));
    double ugol = atan(((ax == x1 ? Double.MAX_VALUE : Math.abs((double) (y1 - ay) / (ax - x1))))) * (quarter % 2 == 0 ? -1 : 1) + (quarter == 4 ? 2 * Math.PI : (quarter != 1 ? Math.PI : 0));
    return (short) (ugol * 180 / Math.PI);
  }

  public static double atan(double x) {
    double SQRT3 = Math.sqrt(3);
    boolean signChange = false;
    boolean Invert = false;
    int sp = 0;
    double x2, a;
    if (x < 0.) {
      x = -x;
      signChange = true;
    }
    if (x > 1.) {
      x = 1 / x;
      Invert = true;
    }
    while (x > Math.PI / 12) {
      sp++;
      a = x + SQRT3;
      a = 1 / a;
      x = x * SQRT3;
      x = x - 1;
      x = x * a;
    }
    x2 = x * x;
    a = x2 + 1.4087812;
    a = 0.55913709 / a;
    a = a + 0.60310579;
    a = a - (x2 * 0.05160454);
    a = a * x;
    while (sp > 0) {
      a = a + Math.PI / 6;
      sp--;
    }
    if (Invert) {
      a = Math.PI / 2 - a;
    }
    if (signChange) {
      a = -a;
    }
    return a;
  }

  public void paint(Graphics g, int offsetX, int offsetY) {
    float angleRadian = (float) (angle * Math.PI / 180);
    switch (level) {
      case 0:
        if (isHeavy)
          drawGunCannon(g, x + offsetX, y + offsetY, angleRadian, fireAnim!=0);
        else
          drawGunPistol(g, x + offsetX, y + offsetY, angleRadian, fireAnim!=0);
        break;
      case 1:
        if (isHeavy)
          drawGunBigCannon(g, x + offsetX, y + offsetY, angleRadian, fireAnim!=0);
        else
          drawGunMachinegun(g, x + offsetX, y + offsetY, angleRadian, fireAnim!=0);
        break;
      case 2:
        if (isHeavy)
          drawGunBFG(g, x + offsetX, y + offsetY, angleRadian, fireAnim!=0);
        else
          drawGunMinigun(g, x + offsetX, y + offsetY, angleRadian, fireAnim!=0);
        break;
    }
  }

  public static void drawGunPistol(Graphics g, int x, int y, float angle, boolean fire) {
    g.setColor(0xFFFFFF);
    g.drawRoundRect(x, y, 24, 24, 4, 4);
    g.drawLine(x + 12, y + 12, x + 12 + (int) (Math.cos(angle) * 6), y + 12 - (int) (Math.sin(angle) * 6));
    g.fillArc(x + 8, y + 8, 8, 8, 0, 360);
    if (fire) {
      g.setColor(0xFF0000);
      g.fillRect(x + 12 + (int) (Math.cos(angle) * 6), y + 12 - (int) (Math.sin(angle) * 6), 1, 1);
    }
  }

  public static void drawGunMachinegun(Graphics g, int x, int y, float angle, boolean fire) {
    g.setColor(0xFFFFFF);
    g.drawRoundRect(x, y, 24, 24, 4, 4);
    g.drawLine(x + 12, y + 12, x + 12 + (int) (Math.cos(angle) * 10), y + 12 - (int) (Math.sin(angle) * 10));
    g.fillArc(x + 8, y + 8, 8, 8, 0, 360);
    if (fire) {
      g.setColor(0xFF0000);
      g.fillRect(x + 12 + (int) (Math.cos(angle) * 10), y + 12 - (int) (Math.sin(angle) * 10), 1, 1);
    }
  }

  public static void drawGunMinigun(Graphics g, int x, int y, float angle, boolean fire) {
    g.setColor(0xFFFFFF);
    g.drawRoundRect(x, y, 24, 24, 4, 4);
    g.drawLine(x + 12 + (int) (Math.cos(angle + Math.PI / 2) * 3), y + 12 - (int) (Math.sin(angle + Math.PI / 2) * 3), x + 12 + (int) (Math.cos(angle) * 10 + Math.cos(angle + Math.PI / 2) * 3), y + 12 - (int) (Math.sin(angle) * 10 + Math.sin(angle + Math.PI / 2) * 3));
    g.drawLine(x + 12 + (int) (Math.cos(angle - Math.PI / 2) * 3), y + 12 - (int) (Math.sin(angle - Math.PI / 2) * 3), x + 12 + (int) (Math.cos(angle) * 10 + Math.cos(angle - Math.PI / 2) * 3), y + 12 - (int) (Math.sin(angle) * 10 + Math.sin(angle - Math.PI / 2) * 3));
    g.fillArc(x + 8, y + 8, 8, 8, 0, 360);
    if (fire) {
      g.setColor(0xFF0000);
      g.fillRect(x + 12 + (int) (Math.cos(angle) * 10 + Math.cos(angle + Math.PI / 2) * 3), y + 12 - (int) (Math.sin(angle) * 10 + Math.sin(angle + Math.PI / 2) * 3), 1, 1);
      g.fillRect(x + 12 + (int) (Math.cos(angle) * 10 + Math.cos(angle - Math.PI / 2) * 3), y + 12 - (int) (Math.sin(angle) * 10 + Math.sin(angle - Math.PI / 2) * 3), 1, 1);
    }
  }

  public static void drawGunCannon(Graphics g, int x, int y, float angle, boolean fire) {
    g.setColor(0xFFFFFF);
    g.drawRoundRect(x, y, 24, 24, 4, 4);
    g.drawLine(x + 12 + (int) (Math.cos(angle + Math.PI / 2) * 3), y + 12 - (int) (Math.sin(angle + Math.PI / 2) * 3), x + 12 + (int) (Math.cos(angle) * 6 + Math.cos(angle + Math.PI / 2) * 3), y + 12 - (int) (Math.sin(angle) * 6 + Math.sin(angle + Math.PI / 2) * 3));
    g.drawLine(x + 12 + (int) (Math.cos(angle - Math.PI / 2) * 3), y + 12 - (int) (Math.sin(angle - Math.PI / 2) * 3), x + 12 + (int) (Math.cos(angle) * 6 + Math.cos(angle - Math.PI / 2) * 3), y + 12 - (int) (Math.sin(angle) * 6 + Math.sin(angle - Math.PI / 2) * 3));
    g.fillArc(x + 8, y + 8, 8, 8, 0, 360);
    if (fire)
      g.setColor(0xFF0000);
    g.drawLine(x + 12 + (int) (Math.cos(angle) * 6 + Math.cos(angle + Math.PI / 2) * 3), y + 12 - (int) (Math.sin(angle) * 6 + Math.sin(angle + Math.PI / 2) * 3), x + 12 + (int) (Math.cos(angle) * 6 + Math.cos(angle - Math.PI / 2) * 3), y + 12 - (int) (Math.sin(angle) * 6 + Math.sin(angle - Math.PI / 2) * 3));
  }

  public static void drawGunBigCannon(Graphics g, int x, int y, float angle, boolean fire) {
    g.setColor(0xFFFFFF);
    g.drawRoundRect(x, y, 24, 24, 4, 4);
    g.drawLine(x + 12 + (int) (Math.cos(angle + Math.PI / 2) * 3), y + 12 - (int) (Math.sin(angle + Math.PI / 2) * 3), x + 12 + (int) (Math.cos(angle) * 10 + Math.cos(angle + Math.PI / 2) * 3), y + 12 - (int) (Math.sin(angle) * 10 + Math.sin(angle + Math.PI / 2) * 3));
    g.drawLine(x + 12 + (int) (Math.cos(angle - Math.PI / 2) * 3), y + 12 - (int) (Math.sin(angle - Math.PI / 2) * 3), x + 12 + (int) (Math.cos(angle) * 10 + Math.cos(angle - Math.PI / 2) * 3), y + 12 - (int) (Math.sin(angle) * 10 + Math.sin(angle - Math.PI / 2) * 3));
    g.fillArc(x + 8, y + 8, 8, 8, 0, 360);
    if (fire)
      g.setColor(0xFF0000);
    g.drawLine(x + 12 + (int) (Math.cos(angle) * 10 + Math.cos(angle + Math.PI / 2) * 3), y + 12 - (int) (Math.sin(angle) * 10 + Math.sin(angle + Math.PI / 2) * 3), x + 12 + (int) (Math.cos(angle) * 10 + Math.cos(angle - Math.PI / 2) * 3), y + 12 - (int) (Math.sin(angle) * 10 + Math.sin(angle - Math.PI / 2) * 3));
  }

  public static void drawGunBFG(Graphics g, int x, int y, float angle, boolean fire) {
    g.setColor(0xFFFFFF);
    g.drawRoundRect(x, y, 24, 24, 4, 4);
    g.drawLine(x + 12 + (int) (Math.cos(angle + Math.PI / 2) * 3), y + 12 - (int) (Math.sin(angle + Math.PI / 2) * 3), x + 12 + (int) (Math.cos(angle) * 10 + Math.cos(angle + Math.PI / 2) * 4), y + 12 - (int) (Math.sin(angle) * 10 + Math.sin(angle + Math.PI / 2) * 4));
    g.drawLine(x + 12 + (int) (Math.cos(angle - Math.PI / 2) * 3), y + 12 - (int) (Math.sin(angle - Math.PI / 2) * 3), x + 12 + (int) (Math.cos(angle) * 10 + Math.cos(angle - Math.PI / 2) * 4), y + 12 - (int) (Math.sin(angle) * 10 + Math.sin(angle - Math.PI / 2) * 4));
    g.fillArc(x + 8, y + 8, 8, 8, 0, 360);
    if (fire)
      g.setColor(0xFF0000);
    g.drawLine(x + 12 + (int) (Math.cos(angle) * 10 + Math.cos(angle + Math.PI / 2) * 4), y + 12 - (int) (Math.sin(angle) * 10 + Math.sin(angle + Math.PI / 2) * 4), x + 12 + (int) (Math.cos(angle) * 10 + Math.cos(angle - Math.PI / 2) * 4), y + 12 - (int) (Math.sin(angle) * 10 + Math.sin(angle - Math.PI / 2) * 4));
  }
}
