import ru.b1n0m.simpleme.Canvas.MainView;
import ru.b1n0m.simpleme.Canvas.SimpleTouchProcessor;

import javax.microedition.lcdui.Display;
import javax.microedition.midlet.MIDlet;

public class Midlet extends MIDlet {

  public static Midlet midlet;
  public MainView mainView;
  public Display myDisplay;

  public Midlet() {
    midlet = this;
  }

  public void startApp() {
    myDisplay = Display.getDisplay(this);
    mainView = new MainView();
    myDisplay.setCurrent(mainView);
    SimpleDefenceEngine view = new SimpleDefenceEngine(mainView);
    mainView.setDrawable(view);
    mainView.setTouchProcessor(view);
    try {
      mainView.registerUpdateTask(true, view);
    } catch (InterruptedException ignored) {}
    mainView.setKeyProcessor(view);
    mainView.start();
  }


  public void pauseApp() {
  }

  public void destroyApp(boolean unconditional) {
    myDisplay.setCurrent(null);
    notifyDestroyed();
  }
}