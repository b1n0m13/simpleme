package ru.b1n0m.simpleme.Canvas;

class UpdateTask {
    protected final boolean onlyShown;
    private final MainView mainView;
    protected final ITask task;

    UpdateTask(boolean onlyShown, MainView mainView, ITask task) {
        this.onlyShown = onlyShown;
        this.mainView = mainView;
        this.task = task;
    }

    protected void run(int keyCode) {
        if (!onlyShown || mainView.isShown()) {
            try {
                task.update(keyCode);
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }
        synchronized (mainView.updateSync) {
            mainView.updates--;
            mainView.updateSync.notifyAll();
        }
    }
}
