package ru.b1n0m.simpleme.Canvas;

public class SimpleTouchProcessor implements ITouchProcessor {
    private final Object ppSyncObj;
    private final short[] ppQueue;
    private static final byte ppQueueCap = 20;
    private byte ppQueueStart, ppQueueEnd, ppQueueCount;
    private short ppOffX, ppOffY, ppPrevDragX, ppPrevDragY;
    private final int[] touchCoord;


    public SimpleTouchProcessor() {
        ppQueue = new short[ppQueueCap];
        ppOffX = ppOffY = ppQueueStart = ppQueueEnd = ppQueueCount = 0;
        ppSyncObj = new Object();
        touchCoord = new int[2];
    }

    protected synchronized int[] getPointerOffset() {
        synchronized (ppSyncObj) {
            touchCoord[0] = ppOffX;
            touchCoord[1] = ppOffY;
            ppOffX = ppOffY = 0;
        }
        return touchCoord;
    }

    public final void pointerTouched(int _x, int _y) {
        ppPrevDragX = (short) _x;
        ppPrevDragY = (short) _y;
    }

    public final void pointerDragged(int _x, int _y, int _prevX, int _prevY) {
        if (ppQueue != null) {
            synchronized (ppSyncObj) {
                ppOffX += _x - ppPrevDragX;
                ppOffY += _y - ppPrevDragY;
                ppPrevDragX = (short) _x;
                ppPrevDragY = (short) _y;
            }
        }
    }

    public final void pointerPressed(int _x, int _y) {
        if (ppQueue != null) {
            if (ppQueueCount < (ppQueueCap >> 1)) {
                ppQueue[ppQueueEnd++] = (short) _x;
                ppQueue[ppQueueEnd++] = (short) _y;
                ppQueueCount++;
                if (ppQueueEnd == ppQueueCap) {
                    ppQueueEnd = 0;
                }
            }
        }
    }

    public synchronized final int[] getPointerPress() {
        if (ppQueueCount < 1) {
            return null;
        }
        touchCoord[0] = ppQueue[ppQueueStart++];
        touchCoord[1] = ppQueue[ppQueueStart++];
        ppQueueCount--;
        if (ppQueueStart == ppQueueCap) {
            ppQueueStart = 0;
        }
        return touchCoord;
    }

    public final void pointerDragBegin() {
    }

    public void initTouch() {}
}
