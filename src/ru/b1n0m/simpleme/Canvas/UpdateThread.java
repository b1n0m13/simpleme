package ru.b1n0m.simpleme.Canvas;

class UpdateThread implements Runnable {
  private boolean shown;
  private MainView mainView;

  public UpdateThread(MainView mainView) {
    super();
    this.mainView = mainView;
  }

  public void run() {
    long startTime, timeTaken;
    mainView.updates = 0;
    while (mainView.isRunning) {
      startTime = System.currentTimeMillis();
      synchronized (mainView.sync) {
        while (mainView.isPainting) {
          try {
            mainView.sync.wait();
          } catch (InterruptedException ignored) {
          }
        }
        mainView.isUpdating = true;
      }
      shown = mainView.isShown();
      mainView.updateKeys();
      mainView.updates = 1;
      update();
      synchronized (mainView.updateSync) {
        while (mainView.updates > 0) {
          try {
            mainView.updateSync.wait();
          } catch (InterruptedException ignored) {
          }
        }
      }
      mainView.keyStatusPhys &= ~mainView.keyStatusPhysUnpressed;
      mainView.keyStatusPhysUnpressed = 0;
      mainView.keyStatus = mainView.keyStatusPhys;
      synchronized (mainView.sync) {
        mainView.isUpdating = false;
        mainView.sync.notifyAll();
      }
      if ((timeTaken = System.currentTimeMillis() - startTime) < MainView.UPDATE_DELAY) {
        try {
          Thread.sleep(MainView.UPDATE_DELAY - timeTaken);
        } catch (InterruptedException ignored) {
        }
      }
    }
    synchronized (mainView.stopSync) {
      mainView.updateThread = null;
      mainView.stopSync.notifyAll();
    }
  }

  public void update() {
    if (mainView.tasks != null) {
      int max = mainView.tasks.length;
      mainView.updates += max;
      for (int i = 0; i != max; ++i) {
        if (!mainView.tasks[i].onlyShown || shown) {
          try {
            mainView.tasks[i].task.update(mainView.keyStatus);
          } catch (Throwable throwable) {
            throwable.printStackTrace();
          }
        }
        mainView.updates--;
      }
    }
    mainView.updates--;
  }
}
