package ru.b1n0m.simpleme.Canvas;

public interface ITask extends IKeyCodes {
    public void update(int keyCode);
}
