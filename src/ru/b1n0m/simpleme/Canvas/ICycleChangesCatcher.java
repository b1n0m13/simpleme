package ru.b1n0m.simpleme.Canvas;

public interface ICycleChangesCatcher {
  public void taskAdded(ITask task, boolean result);

  public void taskRemoved(ITask task, boolean result);

  public void stopped();
}
