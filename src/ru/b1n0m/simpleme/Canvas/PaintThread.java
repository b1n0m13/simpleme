package ru.b1n0m.simpleme.Canvas;

class PaintThread implements Runnable {
    private MainView mainView;

    public PaintThread(MainView mainView) {
        this.mainView = mainView;
    }

    public void run() {
        long startTime, timeTaken;
        while (mainView.isRunning) {
            startTime = System.currentTimeMillis();
            if (mainView.isShown()) {
                synchronized (mainView.sync) {
                    while (mainView.isUpdating) {
                        try {
                            mainView.sync.wait();
                        } catch (InterruptedException ignored) {}
                    }
                    mainView.isPainting = true;
                }
                synchronized (mainView.paintSync) {
                    mainView.waitPaint = true;
                    mainView.repaint();
                    while (mainView.waitPaint) {
                        try {
                            mainView.paintSync.wait();
                        } catch (InterruptedException ignored) {}
                    }
                }
                synchronized (mainView.sync) {
                    mainView.isPainting = false;
                    mainView.sync.notifyAll();
                }
            }
            if ((timeTaken = System.currentTimeMillis() - startTime) < MainView.PAINT_DELAY) {
                try {
                    Thread.sleep(MainView.PAINT_DELAY -timeTaken);
                } catch (InterruptedException ignored) {}
            }
        }
        synchronized (mainView.stopSync) {
            mainView.paintThread = null;
            mainView.stopSync.notifyAll();
        }
    }
}
