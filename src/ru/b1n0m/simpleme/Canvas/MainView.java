package ru.b1n0m.simpleme.Canvas;

import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Graphics;
import java.util.Random;

public class MainView extends Canvas implements IKeyCodes {
    public static volatile int PAINT_DELAY = 40, UPDATE_DELAY = 10;

    protected volatile boolean isRunning, isPainting, isUpdating, waitPaint;
    protected final Object paintSync, updateSync, sync, stopSync;
    protected volatile int updates;
    protected volatile UpdateTask[] tasks;

    public static int KEY_LEFTSOFT;
    public static int KEY_RIGHTSOFT;
    protected static boolean isSKSet;


    protected int keyStatusPhys; //shadow status - физич. статус кнопок
    protected int keyStatus; //статус клавиш для обработки
    protected int keyStatusPhysUnpressed;

    protected int[] keyQueue; //очередь нажатий кнопок
    protected int keyQueueIdx;

    protected int[] pointerTouchedQueue; //очередь касаний на экран
    protected int pointerTouchedQueueIdx;
    protected int[] pointerPressedQueue; //очередь нажатий на экран
    protected int pointerPressedQueueIdx;
    protected int[] pointerDraggedQueue; //очередь движения по экрану
    protected int pointerDraggedQueueIdx;

    protected boolean qFullScreen;

    protected boolean isOwnSizeChanged;

    //for touchscreen
    public boolean bTouchPhone;

    public int xPressed, yPressed;
    protected int xDragged, yDragged;
    protected boolean bDraggedMode;

    protected static final int UNINITIALIZED = -1;
    public int DISPLAY_W, DISPLAY_H;
    protected static final int R = 10;
    protected ITouchProcessor touchPr;
    protected IKeyProcessor keyPr;
    protected IDrawable drawable;
    protected Thread paintThread, updateThread;
    private final String paintThreadName, updateThreadName;

    private static final Random myRnd = new Random();

    public MainView() {
        isRunning = false;
        isUpdating = false;
        paintSync = new Object();
        updateSync = new Object();
        stopSync = new Object();
        sync = new Object();
        tasks = null;

        keyQueue = new int[2];
        DISPLAY_W = UNINITIALIZED;
        DISPLAY_H = UNINITIALIZED;
        qFullScreen = false;
        isOwnSizeChanged = false;
        bTouchPhone = false;

        paintThreadName = "paintThread";
        updateThreadName = "updateThread";
    }

    public void start() {
        clearKeyStatus();
        paintThread = new Thread(new PaintThread(this), paintThreadName);
        updateThread = new Thread(new UpdateThread(this), updateThreadName);
        isRunning = true;
        updateThread.start();
        paintThread.start();
    }

    public void asyncStop(final ICycleChangesCatcher catcher) {
      new Thread(){public void run() {
        try {
          stop();
        } catch (InterruptedException ignored) {}
        if (catcher != null)
          catcher.stopped();
      }}.start();
    }

    public void stop() throws InterruptedException {
        String threadName = Thread.currentThread().getName();
        if (paintThreadName.equals(threadName) || updateThreadName.equals(threadName))
          throw new InterruptedException("Cannot run this method inside paint or update. Use asyncStop instead.");
        synchronized (stopSync) {
            isRunning = false;
            if (updateThread != null || paintThread != null) {
                try {
                    stopSync.wait();
                } catch (InterruptedException ignored) {}
            }
        }
        clearKeyStatus();
    }

    public void setTouchProcessor(ITouchProcessor tp) {
        this.touchPr = tp;
    }

    public void setKeyProcessor(IKeyProcessor kp) {
        this.keyPr = kp;
    }

    public void setDrawable(IDrawable p) {
        this.drawable = p;
    }

    protected void paint(Graphics graphics) {
        if (!qFullScreen) {
            qFullScreen = true;
            setFullScreenMode(true);
            isOwnSizeChanged = true;
        }
        if (isOwnSizeChanged) {
            isOwnSizeChanged = false;
            DISPLAY_W = getWidth();
            DISPLAY_H = getHeight();
        }
        try {
            if (drawable != null) {
                drawable.paint(graphics);
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        synchronized (paintSync) {
            waitPaint = false;
            paintSync.notifyAll();
        }
    }

    protected void showNotify() {
        super.showNotify();
        clearKeyStatus();
        if (drawable != null) {
            drawable.showNotify();
        }
    }

    protected void hideNotify() {
        super.hideNotify();
        clearKeyStatus();
        if (drawable != null) {
            drawable.hideNotify();
        }
    }

    public void asyncRegisterUpdateTask(final boolean onlyShown, final ITask task, final ICycleChangesCatcher catcher) {
      new Thread(){public void run() {
        boolean registered = false;
        try {
          registered = registerUpdateTask(onlyShown, task);
        } catch (InterruptedException ignored) {}
        if (catcher != null)
          catcher.taskAdded(task, registered);
      }}.start();
    }

    public boolean registerUpdateTask(boolean onlyShown, ITask task) throws InterruptedException {
        String threadName = Thread.currentThread().getName();
        if (paintThreadName.equals(threadName) || updateThreadName.equals(threadName))
          throw new InterruptedException("Cannot run this method inside paint or update. Use asyncRegisterUpdateTask instead.");
        if (task == null) {
            return false;
        }
        removeUpdateTask(task);
        synchronized (updateSync) {
            UpdateTask ut = new UpdateTask(onlyShown, this, task);
            UpdateTask[] arr = new UpdateTask[tasks == null? 1 : tasks.length + 1];
            arr[arr.length-1] = ut;
            if (tasks != null) {
                System.arraycopy(tasks, 0, arr, 0, tasks.length);
            }
            tasks = arr;
        }
        return true;
    }

    public void asyncRemoveUpdateTask(final ITask task, final ICycleChangesCatcher catcher) {
      new Thread(){public void run() {
        boolean removed = false;
        try {
          removed = removeUpdateTask(task);
        } catch (InterruptedException ignored) {}
        if (catcher != null)
          catcher.taskRemoved(task, removed);
      }}.start();
    }

    public boolean removeUpdateTask(ITask task) throws InterruptedException {
        String threadName = Thread.currentThread().getName();
        if (paintThreadName.equals(threadName) || updateThreadName.equals(threadName))
          throw new InterruptedException("Cannot run this method inside paint or update. Use asyncRemoveUpdateTask instead.");
        if (task == null || tasks == null) {
            return false;
        }
        synchronized (updateSync) {
            for (int i = 0, max = tasks.length; i != max; ++i) {
              if (tasks[i].task == task){
                if (tasks.length == 1)
                  tasks = null;
                else {
                  UpdateTask[] arr = new UpdateTask[tasks.length - 1];
                  System.arraycopy(tasks, 0, arr, 0, i);
                  System.arraycopy(tasks, i+1, arr, i, max-i-1);
                  tasks = arr;
                }
                return true;
              }
            }
        }
      return false;
    }

    public final boolean isTouchPhone() {
        return bTouchPhone;
    }

    protected final void keyPressed(int keyCode) {
        if (keyQueueIdx != keyQueue.length) {
            keyQueue[keyQueueIdx++] = getKey(keyCode, this);
        }
    }

    protected final void keyRepeated(int keyCode) {

    }

    protected final void keyReleased(int keyCode) {
        keyStatusPhysUnpressed |= getKey(keyCode, this);
    }

    protected void pointerPressed(int x, int y) {
        if (!bTouchPhone) {
            bTouchPhone = true;
            if (touchPr != null) {
                touchPr.initTouch();
            }

            pointerTouchedQueue = new int[2];
            pointerPressedQueue = new int[4];
            pointerDraggedQueue = new int[64]; //16 послед. точек
        }
        xPressed = x;
        yPressed = y;
        xDragged = x;
        yDragged = y;
        bDraggedMode = false;

        if (pointerTouchedQueueIdx != pointerTouchedQueue.length) {
            pointerTouchedQueue[pointerTouchedQueueIdx + 0] = xPressed;
            pointerTouchedQueue[pointerTouchedQueueIdx + 1] = yPressed;
            pointerTouchedQueueIdx += 2;
        }
    }

    protected void pointerReleased(int x, int y) {
        int xReleased = x;
        int yReleased = y;
        //при малом перемещении от точки нажатия генерировать нажатие
        if ((xPressed - xReleased) * (xPressed - xReleased) + (yPressed - yReleased) * (yPressed - yReleased) <= R * R) {
            if (pointerPressedQueueIdx != pointerPressedQueue.length) {
                pointerPressedQueue[pointerPressedQueueIdx + 0] = xPressed;
                pointerPressedQueue[pointerPressedQueueIdx + 1] = yPressed;
                pointerPressedQueueIdx += 2;
            }
        }
    }

    protected void pointerDragged(int x, int y) {
        int xPrevDragged = xDragged;
        int yPrevDragged = yDragged;
        xDragged = x;
        yDragged = y;

        //генерировать перемещения только при превышении предела
        if (!bDraggedMode) {
            if ((xPressed - xDragged) * (xPressed - xDragged) + (yPressed - yDragged) * (yPressed - yDragged) >= R * R) {
                xPressed = x; //drag начался, точкой начала будем считать эту
                yPressed = y;
                if (touchPr != null)
                    touchPr.pointerDragBegin();
                bDraggedMode = true;
            }
        } else {
            if (pointerDraggedQueueIdx != pointerDraggedQueue.length) {
                pointerDraggedQueue[pointerDraggedQueueIdx + 0] = xDragged;
                pointerDraggedQueue[pointerDraggedQueueIdx + 1] = yDragged;
                pointerDraggedQueue[pointerDraggedQueueIdx + 2] = xPrevDragged;
                pointerDraggedQueue[pointerDraggedQueueIdx + 3] = yPrevDragged;
                pointerDraggedQueueIdx += 4;
            }
        }
    }

    protected void updateKeys() {
        int keyCode;
        while (keyQueueIdx > 0) {
            keyCode = keyQueue[--keyQueueIdx];

            if (keyPr != null && keyPr.keyCodePressed(keyCode))
                continue; //если дочерняя канва обрабатывает нажатие, то более ничего не делаем


            keyStatusPhys |= keyCode;
            switch (keyCode) {
                case KEY_CODE_LEFT:
                case KEY_CODE_PAD_LEFT:
                    keyStatusPhys &= ~KEY_CODE_RIGHT;
                    keyStatusPhys &= ~KEY_CODE_PAD_RIGHT;
                    break;

                case KEY_CODE_RIGHT:
                case KEY_CODE_PAD_RIGHT:
                    keyStatusPhys &= ~KEY_CODE_LEFT;
                    keyStatusPhys &= ~KEY_CODE_PAD_LEFT;
                    break;

                case KEY_CODE_UP:
                case KEY_CODE_PAD_UP:
                    keyStatusPhys &= ~KEY_CODE_DOWN;
                    keyStatusPhys &= ~KEY_CODE_PAD_DOWN;
                    break;

                case KEY_CODE_DOWN:
                case KEY_CODE_PAD_DOWN:
                    keyStatusPhys &= ~KEY_CODE_UP;
                    keyStatusPhys &= ~KEY_CODE_PAD_UP;
                    break;

                case KEY_CODE_UP_LEFT:
                    keyStatusPhys &= ~KEY_CODE_DOWN_RIGHT;
                    break;

                case KEY_CODE_UP_RIGHT:
                    keyStatusPhys &= ~KEY_CODE_DOWN_LEFT;
                    break;

                case KEY_CODE_DOWN_LEFT:
                    keyStatusPhys &= ~KEY_CODE_UP_RIGHT;
                    break;

                case KEY_CODE_DOWN_RIGHT:
                    keyStatusPhys &= ~KEY_CODE_UP_LEFT;
                    break;
            }

            keyStatus = keyStatusPhys;
        }

        while (pointerDraggedQueueIdx > 0 || pointerPressedQueueIdx > 0 || pointerTouchedQueueIdx > 0) {
            if (pointerTouchedQueueIdx > 0) {
                pointerTouchedQueueIdx -= 2;
                if (touchPr != null)
                    touchPr.pointerTouched(pointerTouchedQueue[pointerTouchedQueueIdx], pointerTouchedQueue[pointerTouchedQueueIdx + 1]);
            }

            if (pointerDraggedQueueIdx > 0) {
                pointerDraggedQueueIdx -= 4;
                if (touchPr != null)
                    touchPr.pointerDragged(pointerDraggedQueue[pointerDraggedQueueIdx], pointerDraggedQueue[pointerDraggedQueueIdx + 1], pointerDraggedQueue[pointerDraggedQueueIdx + 2], pointerDraggedQueue[pointerDraggedQueueIdx + 3]);
            }

            if (pointerPressedQueueIdx > 0) {
                pointerPressedQueueIdx -= 2;
                if (touchPr != null)
                    touchPr.pointerPressed(pointerPressedQueue[pointerPressedQueueIdx], pointerPressedQueue[pointerPressedQueueIdx + 1]);
            }
        }
    }

    public void clearKeyStatus() {
        keyQueueIdx = 0;
        keyStatus = 0;
        keyStatusPhysUnpressed = 0xFFFFFFFF; //все кнопки отжаты
    }

    protected final void sizeChanged(int w, int h) {
        super.sizeChanged(w, h);
        isOwnSizeChanged = true;
    }


    private static int getKey(int keyCode, Canvas c) {
        if (!isSKSet) {
            setKnownSoftKeyCodes(c);
            isSKSet = true;
        }

        switch (keyCode) {
            case Canvas.KEY_NUM5:
                return KEY_CODE_FIRE;

            case Canvas.KEY_NUM2:
                return KEY_CODE_UP;

            case Canvas.KEY_NUM8:
                return KEY_CODE_DOWN;

            case Canvas.KEY_NUM4:
                return KEY_CODE_LEFT;

            case Canvas.KEY_NUM6:
                return KEY_CODE_RIGHT;

            case Canvas.KEY_NUM1:
                return KEY_CODE_UP_LEFT;

            case Canvas.KEY_NUM3:
                return KEY_CODE_UP_RIGHT;

            case Canvas.KEY_NUM7:
                return KEY_CODE_DOWN_LEFT;

            case Canvas.KEY_NUM9:
                return KEY_CODE_DOWN_RIGHT;

            case Canvas.KEY_NUM0:
                return KEY_CODE_0;

            case Canvas.KEY_STAR:
                return KEY_CODE_STAR;

            case Canvas.KEY_POUND:
                return KEY_CODE_POUND;

            default:
                if (keyCode == KEY_LEFTSOFT) {
                    return KEY_CODE_SK_LEFT;
                } else if (keyCode == KEY_RIGHTSOFT) {
                    return KEY_CODE_SK_RIGHT;
                } else {
                    int gameAction = c.getGameAction(keyCode);
                    switch (gameAction) {
                        case Canvas.UP:
                            return KEY_CODE_PAD_UP;

                        case Canvas.DOWN:
                            return KEY_CODE_PAD_DOWN;

                        case Canvas.LEFT:
                            return KEY_CODE_PAD_LEFT;

                        case Canvas.RIGHT:
                            return KEY_CODE_PAD_RIGHT;

                        case Canvas.FIRE:
                            return KEY_CODE_FIRE;

                        default:
                            return KEY_CODE_NONE;
                    }
                }
        }
    }

    private static void setKnownSoftKeyCodes(Canvas canvas) {
        try {
            Class.forName("com.siemens.mp.game.Light");
            KEY_LEFTSOFT = -1;
            KEY_RIGHTSOFT = -4;
            //Form.clearSK = -12;
            return;
        } catch (ClassNotFoundException ignored) {
        }

        try {
            Class.forName("com.motorola.phonebook.PhoneBookRecord");
            KEY_LEFTSOFT = -21;
            KEY_RIGHTSOFT = -22;
            return;
            //maybe Motorola A1000 has different keyCodes
            //Left soft key: Key code -10,
            //Right soft key: Key code -11,
        } catch (ClassNotFoundException ignored) {
        }

        try {
            Class.forName("com.nokia.mid.ui.FullCanvas");
            KEY_LEFTSOFT = -6;
            KEY_RIGHTSOFT = -7;
            return;
        } catch (ClassNotFoundException ignored) {
        }

        try {
            Class.forName("net.rim.device.api.system.Application");
            //there are no soft keys on the Blackberry
            //instead use the Q and P keys
            KEY_LEFTSOFT = /*Keypad.KEY_MENU;/*/113;
            KEY_RIGHTSOFT = /*Keypad.KEY_ESCAPE;/*/112;
            //some BB devices use O as the most right key
            //Form.rightSK2 = 111;
            // Form.clearSK = 8;
            return;
        } catch (ClassNotFoundException ignored) {
        }

        // detecting LG
        try {
            // identical device
            Class.forName("com.mot.iden.util.Base64");
            //Form.clearSK = -5000;
            KEY_LEFTSOFT = -20;
            KEY_RIGHTSOFT = -21;
            // Display.getInstance().setFireValue(-23);
            return;
        } catch (Throwable ignored) {
        }

        try {
            Class.forName("mmpp.media.MediaPlayer");
            //Form.clearSK = -204;
        } catch (ClassNotFoundException ignored) {
        }

        try {
            if (canvas.getKeyName(-6).toUpperCase().indexOf("SOFT") >= 0) {
                KEY_LEFTSOFT = -6;
                KEY_RIGHTSOFT = -7;
                return;
            }
            if (canvas.getKeyName(21).toUpperCase().indexOf("SOFT") >= 0) {
                KEY_LEFTSOFT = 21;
                KEY_RIGHTSOFT = 22;
                return;
            }
        } catch (Exception ignored) {
        }


        boolean leftInit = false;
        boolean rightInit = false;
        for (int i = -127; i < 127; i++) {
            if (leftInit && rightInit) {  //I have added this if to avoid unnecessary loops
                return; //but the main reason is that sometimes after the correct negative values were initialized also positive
            }          // key codes had "soft" in the name.
            try {
                if (canvas.getKeyName(i).toUpperCase().indexOf("SOFT") < 0) continue;
                if (canvas.getKeyName(i).indexOf('1') >= 0) {
                    KEY_LEFTSOFT = i;
                    leftInit = true;
                }
                if (canvas.getKeyName(i).indexOf('2') >= 0) {
                    KEY_RIGHTSOFT = i;
                    rightInit = true;
                }
            } catch (Exception ignored) {
            }
        }
    }

    public static int getRandomInt(int min, int max) {
    if (min == max) {
      return min;
    } else {
      return min + Math.abs(myRnd.nextInt()) % (max - min + 1);
    }
  }

  public static double getRandomDouble(double min, double max) {
    if (Math.abs(min - max) < 0.0000001) {
      return min;
    } else {
      return min + Math.abs(myRnd.nextDouble()) % (max - min);
    }
  }
}
