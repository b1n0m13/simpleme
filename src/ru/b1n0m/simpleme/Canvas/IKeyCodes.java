package ru.b1n0m.simpleme.Canvas;

public interface IKeyCodes {
  public final static int KEY_CODE_NONE         = 0x000000;
  public final static int KEY_CODE_FIRE         = 0x000001;
  public final static int KEY_CODE_UP           = 0x000002; //код цифровой клавиатуры
  public final static int KEY_CODE_DOWN         = 0x000004; //код цифровой клавиатуры
  public final static int KEY_CODE_LEFT         = 0x000008; //код цифровой клавиатуры
  public final static int KEY_CODE_RIGHT        = 0x000010; //код цифровой клавиатуры
  public final static int KEY_CODE_UP_LEFT      = 0x000020;
  public final static int KEY_CODE_UP_RIGHT     = 0x000040;
  public final static int KEY_CODE_DOWN_LEFT    = 0x000080;
  public final static int KEY_CODE_DOWN_RIGHT   = 0x000100;
  public final static int KEY_CODE_0            = 0x000200;
  public final static int KEY_CODE_STAR         = 0x000400;
  public final static int KEY_CODE_POUND        = 0x000800;
  public final static int KEY_CODE_SK_LEFT      = 0x001000;
  public final static int KEY_CODE_SK_RIGHT     = 0x002000;
  //коды джойстика
  public final static int KEY_CODE_PAD_UP       = 0x004000; //код джойстика
  public final static int KEY_CODE_PAD_DOWN     = 0x008000; //код джойстика
  public final static int KEY_CODE_PAD_LEFT     = 0x010000; //код джойстика
  public final static int KEY_CODE_PAD_RIGHT    = 0x020000; //код джойстика

  public final static int KEY_CODE_SK_MIDDLE    = KEY_CODE_FIRE;
  public final static int KEY_CODE_SOME_SK_CODE = KEY_CODE_SK_LEFT | KEY_CODE_SK_MIDDLE | KEY_CODE_SK_RIGHT;

  //аналоги кода в цифровом виде
  public final static int KEY_CODE_1            = KEY_CODE_UP_LEFT; 
  public final static int KEY_CODE_2            = KEY_CODE_UP;
  public final static int KEY_CODE_3            = KEY_CODE_UP_RIGHT;
  public final static int KEY_CODE_4            = KEY_CODE_LEFT;
  public final static int KEY_CODE_5            = KEY_CODE_FIRE;
  public final static int KEY_CODE_6            = KEY_CODE_RIGHT;
  public final static int KEY_CODE_7            = KEY_CODE_DOWN_LEFT;
  public final static int KEY_CODE_8            = KEY_CODE_DOWN;
  public final static int KEY_CODE_9            = KEY_CODE_DOWN_RIGHT;

}
