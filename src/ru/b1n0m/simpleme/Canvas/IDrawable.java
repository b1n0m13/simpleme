package ru.b1n0m.simpleme.Canvas;

import javax.microedition.lcdui.Graphics;

public interface IDrawable {
    public void showNotify();
    public void hideNotify();
    public void paint(Graphics graphics);
}
