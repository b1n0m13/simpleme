package ru.b1n0m.simpleme.Canvas;

public interface IKeyProcessor extends IKeyCodes {
    public boolean keyCodePressed(int keyCode);
}
