package ru.b1n0m.simpleme.Canvas;

public interface ITouchProcessor {
    public void pointerDragBegin();

    public void pointerTouched(int x, int y);

    public void pointerDragged(int x, int y, int x2, int y2);

    public void pointerPressed(int x, int y);

    public void initTouch();
}
