import javax.microedition.lcdui.Image;

public class Tools {
  public static Image smoothBorders(Image img, byte trns, int coef) {
    int w = img.getWidth();
    int h = img.getHeight();
    int[] raw = getRGB(img);//new int[w * h];
    //img.getRGB(raw, 0, w, 0, 0, w, h);
    int transp = trns << 24;
    byte x;
    int coef_min1 = coef - 1;
    //углы
    if ((raw[0] & 0xFF000000) == 0xFF000000) {//левый верхний
      x = 2;
      if ((raw[1] & 0xFF000000) == 0) x++;
      if ((raw[w] & 0xFF000000) == 0) x++;
      if (x > coef_min1) {
        raw[0] = raw[0] & 0xFFFFFF | transp;
      }
    }
    if ((raw[w - 1] & 0xFF000000) == 0xFF000000) {//правый верхний
      x = 2;
      if ((raw[w - 2] & 0xFF000000) == 0) x++;
      if ((raw[(w << 1) - 1] & 0xFF000000) == 0) x++;
      if (x > coef_min1) {
        raw[w - 1] = raw[w - 1] & 0xFFFFFF | transp;
      }
    }
    if ((raw[(h - 1) * w] & 0xFF000000) == 0xFF000000) {//левый нижний
      x = 2;
      if ((raw[(h - 2) * w] & 0xFF000000) == 0) x++;
      if ((raw[1 + (h - 1) * w] & 0xFF000000) == 0) x++;
      if (x > coef_min1) {
        raw[(h - 1) * w] = raw[(h - 1) * w] & 0xFFFFFF | transp;
      }
    }
    if ((raw[w - 1 + (h - 1) * w] & 0xFF000000) == 0xFF000000) {//правый нижний
      x = 2;
      if ((raw[w - 1 + (h - 2) * w] & 0xFF000000) == 0) x++;
      if ((raw[w - 2 + (h - 1) * w] & 0xFF000000) == 0) x++;
      if (x > coef_min1) {
        raw[w - 1 + (h - 1) * w] = raw[w - 1 + (h - 1) * w] & 0xFFFFFF | transp;
      }
    }
    //края
    for (int a = 1; a < w - 2; a++) {//верх
      if ((raw[a] & 0xFF000000) == 0xFF000000) {
        x = 1;
        if ((raw[a - 1] & 0xFF000000) == 0) x++;
        if ((raw[a + 1] & 0xFF000000) == 0) x++;
        if ((raw[a + w] & 0xFF000000) == 0) x++;
        if (x > coef_min1) {
          raw[a] = raw[a] & 0xFFFFFF | transp;
        }
      }
    }
    for (int a = 1; a < w - 2; a++) {//низ
      if ((raw[a + (h - 1) * w] & 0xFF000000) == 0xFF000000) {
        x = 1;
        if ((raw[a + (h - 2) * w] & 0xFF000000) == 0) x++;
        if ((raw[a - 1 + (h - 1) * w] & 0xFF000000) == 0) x++;
        if ((raw[a + 1 + (h - 1) * w] & 0xFF000000) == 0) x++;
        if (x > coef_min1) {
          raw[a + (h - 1) * w] = raw[a + (h - 1) * w] & 0xFFFFFF | transp;
        }
      }
    }
    for (int b = 1; b < h - 2; b++) {//лево
      if ((raw[b * w] & 0xFF000000) == 0xFF000000) {
        x = 1;
        if ((raw[(b - 1) * w] & 0xFF000000) == 0) x++;
        if ((raw[1 + b * w] & 0xFF000000) == 0) x++;
        if ((raw[(b + 1) * w] & 0xFF000000) == 0) x++;
        if (x > coef_min1) {
          raw[b * w] = raw[b * w] & 0xFFFFFF | transp;
        }
      }
    }
    for (int b = 1; b < h - 2; b++) {//право
      if ((raw[w - 2 + b * w] & 0xFF000000) == 0xFF000000) {
        x = 1;
        if ((raw[w - 2 + (b - 1) * w] & 0xFF000000) == 0) x++;
        if ((raw[w - 3 + b * w] & 0xFF000000) == 0) x++;
        if ((raw[w - 2 + (b + 1) * w] & 0xFF000000) == 0) x++;
        if (x > coef_min1) {
          raw[w - 2 + b * w] = raw[w - 2 + b * w] & 0xFFFFFF | transp;
        }
      }
    }
    //центр картинки
    for (int a = 1; a < w - 1; a++) {
      for (int b = 1; b < h - 1; b++) {
        if ((raw[a + b * w] & 0xFF000000) == 0xFF000000) {
          x = 0;
          if ((raw[a + (b - 1) * w] & 0xFF000000) == 0) x++;
          if ((raw[a - 1 + b * w] & 0xFF000000) == 0) x++;
          if ((raw[a + 1 + b * w] & 0xFF000000) == 0) x++;
          if ((raw[a + (b + 1) * w] & 0xFF000000) == 0) x++;
          if (x > coef_min1) {
            raw[a + b * w] = raw[a + b * w] & 0xFFFFFF | transp;
          }
        }
      }
    }
    return Image.createRGBImage(raw, w, h, true);
  }

  public static int[] getRGB(Image image) {
    int w = image.getWidth();
    int h = image.getHeight();
    int[] raw = new int[w * h];
    image.getRGB(raw, 0, w, 0, 0, w, h);
    return raw;
  }

  public static Image recoverTransparency(Image img, int baseColor) {
    int w = img.getWidth();
    int h = img.getHeight();
    int[] raw = new int[w * h];
    img.getRGB(raw, 0, w, 0, 0, w, h);
    for (int i = 0; i < raw.length; i++) {
      raw[i] = ((raw[i] & 0xFF) << 24) + baseColor;
    }
    return Image.createRGBImage(raw, w, h, true);
  }
}
