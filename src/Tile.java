import javax.microedition.lcdui.Graphics;

public class Tile {

  public static final int SIZE = 25;

  public static final byte TYPE_NONE = 0;
  public static final byte TYPE_SPACE = 1;
  public static final byte TYPE_ROAD = 2;
  public static final byte TYPE_BASE = 4;
  public static final byte TYPE_SPAWN = 8;

  private short x, y;
  byte type;
  private int color;
  private Turret turret;
  private byte directions;

  public Tile(short x, short y, byte type) {
    this.x = x;
    this.y = y;
    this.type = type;
    switch (this.type) {
      case TYPE_SPACE:
        color = 0x1F1F1F;
        break;
      case TYPE_SPAWN:
      case TYPE_ROAD:
        color = 0x3F3F3F;
        break;
      case TYPE_BASE:
        color = 0x7F7F7F;
        break;
    }
  }

  public void paint(Graphics gr, int offsetX, int offsetY) {
    gr.setColor(color);
    if (type != TYPE_NONE)
    {gr.fillRect(x + offsetX, y + offsetY, SIZE, SIZE);
      if(turret != null)
        turret.paint(gr, offsetX, offsetY);
    }
  }

  public Turret getTurret() {
    return turret;
  }

  public Turret buildTurret(boolean heavy) {
    if (turret == null)
      turret = new Turret(x, y, heavy);
    return turret;
  }

  public void setGoRight(boolean b){
    if (b)
      directions |= 0x40;
    else
      directions &= 0xBF;
  }

  public void setGoLeft(boolean b) {
    if (b)
      directions |= 0x80;
    else
      directions &= 0x7F;
  }

  public void setGoUp(boolean b){
    if (b)
      directions |= 0x20;
    else
      directions &= 0xDF;
  }

  public void setGoDown(boolean b){
    if (b)
      directions |= 0x10;
    else
      directions &= 0xEF;
  }
}
