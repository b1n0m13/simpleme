# Simple ME #

This is a very old code. I put it here if for some reason someone will need it. It allows to make games for J2ME (Java Microedition) without spending time to make game cycle, bind keys, etc.
Just keep in mind that it is very old code, maybe one of the first thing I ever wrote.

### What`s inside? ###

* Lib files (package ru.b1n0m.simpleme.Canvas)
* Example of usage (very rough prototype of tower defense game)

### How to use it? ###

* You will need J2ME SDK (midp 2.0) to compile
* Use J2ME phone or emulator to run

### Downloads ###

[Download compiled jar](https://bitbucket.org/b1n0m13/simpleme/downloads/SimpleME.jar)

### Questions? ###

* If you have any questions, email me: b1n0m1987 at gmail.com